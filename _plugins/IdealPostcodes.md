---
layout: plugindetail
name: Ideal Postcodes
strapline: Accurate Address Verification for UK Churches
description: Don't get stuck with an inaccurate Bing location service! Ideal Postcodes provides accurate address verification for throughout the UK using Royal Mail's PAF data set combined with Ordanance Survey and ONS data. This plugin takes full advantage of all of the features provided by Ideal Postcodes.
repo: IdealPostcodes
icon:
rockstore:
documentation: true
---
