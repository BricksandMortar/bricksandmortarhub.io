---
layout: plugindetail
name: SendGrid
strapline: Reliable Email Deliverability with SendGrid
description: SendGrid provides reliable email delivery across the web. Take advantage of click and open tracking and gain insight into your audience.
repo: SendGrid
icon:
rockstore:
documentation:
---
